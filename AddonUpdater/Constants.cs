﻿namespace AddonUpdater
{
    class Constants
    {
        public const string ConfigName = "config.xml";
        public const string OverridePath = "overrides";
    }
}