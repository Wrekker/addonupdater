﻿using System;
using System.Linq;
using AddonUpdater.Configuration;

namespace AddonUpdater
{
    class Program
    {
        static void Main(string[] args)
        {
            var output = Console.Out;
            var input = Console.In;

            output.WriteLine("## Add-on Updater for World of Warcraft ##");
            output.WriteLine();
            
            bool forceUpdate = args.Contains("-force", StringComparer.CurrentCultureIgnoreCase);
            bool configMode = args.Contains("-config", StringComparer.CurrentCultureIgnoreCase);

            AppConfig appConfig = AppConfig.Load(Constants.ConfigName);
            if (appConfig == null) {
                AppConfig.GetDefaults(Constants.ConfigName).Save();
                appConfig = AppConfig.Load(Constants.ConfigName);

                output.WriteLine("No configuration was found. A default one will be created and opened for you.");
                configMode = true;
            }

            var update = true;
            if (configMode)
            {
                new ConsoleConfigurator(output, input).Config(appConfig);

                output.WriteLine();
                output.Write("Update add-ons now? (y/n) ");
                update = char.ToLower((char)input.Read()) == 'y';
                output.WriteLine();
            }

            if (update)
            {
                new ConsoleUpdater(output).Update(appConfig, forceUpdate);

                output.WriteLine();
                output.WriteLine("Done! Press Enter to exit.");

                input.Read();
            }
        }
    }
}
