﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using AddonLib;
using AddonLib.Configuration;
using AddonLib.Sources;
using AddonUpdater.Configuration;

namespace AddonUpdater
{
    using ICSharpCode.SharpZipLib.Zip;


    internal class ConsoleUpdater
    {
        private readonly TextWriter _output;

        public ConsoleUpdater(TextWriter output)
        {
            _output = output;
        }

        public void Update(AppConfig appConfig, bool forceUpdate) {
            if (forceUpdate) {
                _output.WriteLine("-force: Forcing updates!");
                _output.WriteLine();
            }

            _output.WriteLine("Fetching {0} addons:", appConfig.Addons.Count);
            
            IEnumerable<IAddonSource> sources = new IAddonSource[] {
                new SourceCurse(),
                new SourceWowInterface(),
                new SourceTukui(),
            };
            
            var addonsPath = Path.Combine(appConfig.GamePath, "interface\\addons");
            var zipHelper = new ZipHelper(addonsPath);
            foreach (var ao in appConfig.Addons) {
                Addon addon = ao;

                if (!forceUpdate && addon.UpdateMode == UpdateMode.Never) {
                    _output.WriteLine("{0} ignored.", addon.Name);
                    continue;
                }

                var source = sources.FirstOrDefault(f => f.CanHandle(addon));
                if (source == null) {
                    _output.WriteLine("Found no valid source for {0} ({1})!", addon.Name, addon.Url);
                    continue;
                }

                int remoteVersion = source.GetVersion(addon);
                if (!forceUpdate && addon.UpdateMode == UpdateMode.Outdated && remoteVersion <= addon.Version) {
                    _output.WriteLine("{0} already up to date (v. {1}), skipping.", addon.Name, addon.Version);
                    continue;
                }
                addon.Version = remoteVersion;

                _output.Write("Downloading {0}...", addon.Name);
                byte[] data = source.Download(addon, new ConsoleProgressHandler());
                _output.WriteLine();

                string addonPath;
                _output.Write("\tUnpacking {0}...", addon.Name);
                using (var ms = new MemoryStream(data)) {
                    var zip = new ZipFile(ms);

                    addonPath = zipHelper.GetAddonBaseDir(zip);
                    zipHelper.Truncate(zip);
                    zipHelper.Unpack(zip, new ConsoleProgressHandler());
                }
                _output.WriteLine();

                var addonQualPath = Path.Combine(addonsPath, addonPath);
                string overrideQualPath = Path.Combine(Constants.OverridePath, addonPath);

                if (Directory.Exists(overrideQualPath)) {
                    _output.WriteLine("\tCopying overrides...");
                    PathHelper.CopyRecursive(overrideQualPath, addonQualPath);
                }

                _output.WriteLine("\t{0} done.", addon.Name);
            }

            appConfig.Save();
        }
    }
}