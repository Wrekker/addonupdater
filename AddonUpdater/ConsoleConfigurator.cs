﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using AddonLib.Configuration;
using AddonUpdater.Configuration;

namespace AddonUpdater
{
    internal class ConsoleConfigurator
    {
        private readonly TextWriter _output;
        private readonly TextReader _input;

        public ConsoleConfigurator(TextWriter output, TextReader input)
        {
            _output = output;
            _input = input;
        }

        public void Config(AppConfig config) {

            _output.WriteLine("Configuration mode");

            if (!IsValidBinaryDirectory(config.GamePath)) {
                Setup(config);
            }

            PrintCommandList();
            _output.WriteLine();

            do {
                _output.Write("> ");
                var command = _input.ReadLine();

                var parts = command.Split(' ');
                var action = parts[0].ToLower();
                
                switch (action) {
                    case "?":
                        PrintCommandList();
                        break;
                    case "setup":
                        Setup(config);
                        break;
                    case "list":
                        PrintAddons(config.Addons);
                        break;
                    case "add":
                        var added = AddAddon();
                        if (added != null) {
                            config.Addons.Add(added);
                        }
                        break;
                    case "view":
                    case "remove":
                    case "edit":
                    case "reset":
                        int id;
                        if (!int.TryParse(parts[1], out id) || id < 1 || id > config.Addons.Count) {
                            break;
                        }
                        id -= 1;
                        switch (action) {
                            case "view":
                                PrintAddon(config.Addons[id]);
                                break;
                            case "remove":
                                config.Addons.RemoveAt(id);
                                break;
                            case "edit":
                                EditAddon(config.Addons[id]);
                                break;
                            case "reset":
                                ResetAddon(config.Addons[id]);
                                break;
                        }
                        break;
                    case "exit":
                        return;
                }

                config.Save();
                _output.WriteLine("Configuration saved.");
            } while (true);
        }

        private void Setup(AppConfig config) {
            string pathString = config.GamePath;
            do {
                _output.Write("Game path: ");
                SendKeys.SendWait(pathString);
                pathString = _input.ReadLine() ?? "";
            } while (!IsValidBinaryDirectory(pathString));
            _output.WriteLine();

            config.GamePath = pathString;
        }

        private static bool IsValidBinaryDirectory(string directory) {
            return File.Exists(Path.Combine(directory, "Wow.exe"));
        }

        private void PrintAddon(Addon addon) {
            _output.WriteLine("  Display name: {0}", addon.Name);
            _output.WriteLine("  URL:          {0}", addon.Url);
            _output.WriteLine("  Update mode:  {0}", addon.UpdateMode);
            _output.WriteLine("  Version:      {0}", addon.Version);
            _output.WriteLine();
        }

        private static void ResetAddon(Addon addon) {
            addon.UpdateMode = UpdateMode.Outdated;
            addon.Version = 0;
        }

        private void PrintCommandList() {
            _output.WriteLine("Commands:");
            _output.WriteLine("  ?             This command list");
            _output.WriteLine("  setup         Set up global settings");
            _output.WriteLine("  list          List managed addons");
            _output.WriteLine("  view <id>     Print addon info");
            _output.WriteLine("  add           Add a new managed addon");
            _output.WriteLine("  remove <id>   Remove addon");
            _output.WriteLine("  edit <id>     Edit addon");
            _output.WriteLine("  reset <id>    Reset version and update mode of addon");
            _output.WriteLine("  exit          Exit configuration");
            _output.WriteLine();
        }

        private void EditAddon(Addon addon) {
            _output.Write("Display name: ");
            SendKeys.SendWait(addon.Name);
            var nameString = _input.ReadLine();

            _output.Write("URL to addon project page: ");
            SendKeys.SendWait(addon.Url);
            var urlString = _input.ReadLine();

            _output.Write("Update mode <outdated|always|never>: ");
            SendKeys.SendWait(addon.UpdateMode.ToString());
            var modeString = _input.ReadLine();

            _output.WriteLine();

            if (!string.IsNullOrWhiteSpace(nameString)) {
                addon.Name = nameString;
            }

            if (!string.IsNullOrWhiteSpace(urlString)) {
                addon.Url = urlString;
            }

            if (!string.IsNullOrWhiteSpace(modeString)) {
                addon.UpdateMode = (UpdateMode)Enum.Parse(typeof(UpdateMode), modeString, true);
            }
        }

        private Addon AddAddon() {
            _output.Write("Display name: ");
            var nameString = _input.ReadLine();
            _output.Write("URL to addon project page: ");
            var urlString = _input.ReadLine();
            _output.Write("Update mode <outdated|always|never>: ");
            var modeString = _input.ReadLine();
            
            if (string.IsNullOrWhiteSpace(nameString) || string.IsNullOrWhiteSpace(urlString)) {
                _output.WriteLine("Cancelled.");
                _output.WriteLine();
                return null;
            }
            _output.WriteLine();

            if (string.IsNullOrWhiteSpace(modeString)) {
                modeString = UpdateMode.Outdated.ToString();
            }

            var mode = (UpdateMode)Enum.Parse(typeof(UpdateMode), modeString, true);

            return new Addon(nameString, urlString, 0, mode);
        }

        private void PrintAddons(IEnumerable<Addon> addons) {
            int i = 0;
            foreach (var addon in addons) {
                _output.WriteLine("{0})\t{1}", ++i, addon.Name);
            }
        }
    }
}