﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Xml.Serialization;
using AddonLib;
using AddonLib.Configuration;
using Microsoft.Win32;

namespace AddonUpdater.Configuration
{
    public class AppConfig
    {
        [XmlIgnore]
        public string Filename { get; private set; }
        public string GamePath { get; set; }
        public List<Addon> Addons { get; set; }

        public static AppConfig Load(string configName) {
            XmlSerializer serializer = new XmlSerializer(typeof(AppConfig));
            
            if (!File.Exists(configName)) {
                return null;
            }

            using (var fs = new FileStream(configName, FileMode.Open)) {
                AppConfig ret = (AppConfig) serializer.Deserialize(fs);
                ret.Filename = configName;

                return ret;
            }
        }

        public void Save() {
            XmlSerializer serializer = new XmlSerializer(typeof(AppConfig));

            using (FileStream fs = new FileStream(this.Filename, FileMode.Create)) {
                serializer.Serialize(fs, this);
            }
        }

        public static AppConfig GetDefaults(string configPath) {
            string gamePath = (string) Registry.GetValue(
                @"HKEY_LOCAL_MACHINE\SOFTWARE\Blizzard Entertainment\World of Warcraft",
                "InstallPath",
                "C:\\Games\\World of Warcraft");

            return new AppConfig {
                                     Filename = configPath,
                                     Addons = new List<Addon> {
                                                                  new Addon("Example", "http://www.curse.com/addons/wow/example", 0, UpdateMode.Outdated)
                                                              },
                                     GamePath = gamePath
                                 };
        }
    }
}