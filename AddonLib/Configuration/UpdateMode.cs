﻿namespace AddonLib.Configuration
{
    public enum UpdateMode
    {
        Outdated,
        Never,
        Always
    }
}