using System.Collections.Generic;
using System.Xml.Serialization;

namespace AddonLib.Configuration
{
    public class Addon
    {
        [XmlAttribute]
        public string Name { get; set; }
        [XmlAttribute]
        public string Url { get; set; }
        [XmlAttribute]
        public int Version { get; set; }
        [XmlAttribute]
        public UpdateMode UpdateMode { get; set; }

        public Addon() { }
        public Addon(string name, string url, int version, UpdateMode mode) {
            this.Name = name;
            this.Url = url;
            this.Version = version;
            this.UpdateMode = mode;
        }
    }
}