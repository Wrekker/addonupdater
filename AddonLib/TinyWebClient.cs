using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace AddonLib
{
    public class TinyWebClient
    {
        public Encoding Encoding { get; set; }
        public CookieContainer Cookies { get; set; }

        public TinyWebClient() : this(Encoding.UTF8) {}
        public TinyWebClient(Encoding encoding) {
            this.Encoding = encoding;
            this.Cookies = new CookieContainer();
        }

        public ResponseStream GetStream(string url) {
            var response = CreateRequest(url).GetResponse();
            var stream = response.GetResponseStream();
            if (stream == null)
                return new ResponseStream(Stream.Null, 0);

            return new ResponseStream(stream, response.ContentLength);
        }

        public string GetString(string url) {
            using (var sr = new StreamReader(GetStream(url).Stream)) {
                return sr.ReadToEnd();
            }
        }

        public Stream PostStream(string url, IDictionary<string, string> values) {
            var wr = CreateRequest(url);
            
            byte[] data = this.Encoding.GetBytes(FormatQuery(values));

            wr.Method = "POST";
            wr.ContentType = "application/x-www-form-urlencoded";
            wr.ContentLength = data.Length;
            Stream dataStream = wr.GetRequestStream();
            dataStream.Write(data, 0, data.Length);
            dataStream.Close();

            var stream = wr.GetResponse().GetResponseStream();

            return stream ?? Stream.Null;
        }

        public string PostString(string url , IDictionary<string, string> values) {
            using (var sr = new StreamReader(PostStream(url, values))) {
                return sr.ReadToEnd();
            }
        }

        private HttpWebRequest CreateRequest(string url) {
            HttpWebRequest wr = (HttpWebRequest)WebRequest.Create(url);
            wr.CookieContainer = this.Cookies;
            
            return wr;
        }

        private string FormatQuery(IDictionary<string, string> dictionary) {
            return dictionary.Aggregate(new StringBuilder(),
                                        (s, i) => s.Append(s.Length == 0 ? "?" : "&")
                                                    .Append(HttpUtility.UrlEncode(i.Key))
                                                    .Append("=")
                                                    .Append(HttpUtility.UrlEncode(i.Value))).ToString();
        }

    }
}