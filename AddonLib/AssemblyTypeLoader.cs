﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;

namespace AddonLib
{
    class AssemblyTypeLoader : ITypeLoader
    {
        private readonly string _path;
        private readonly string _regexFilter;

        public AssemblyTypeLoader(string path, string regexFilter) {
            _path = path;
            _regexFilter = regexFilter;
        }

        public IEnumerable<Type> LoadTypes(Type implementationsOf) {
            List<Type> ret = new List<Type>();

            foreach (var f in Directory.GetFiles(_path)) {
                if (!Regex.IsMatch(f, _regexFilter)) {
                    continue;
                }

                string filename = Path.Combine(_path, f);

                var ass = Assembly.LoadFrom(filename);
                var types = ass.GetTypes().Where(w => 
                                                 w.GetInterface(implementationsOf.FullName) != null &&
                                                 !w.IsAbstract
                    );

                ret.AddRange(types);
            }

            return ret;
        }


    }
}