﻿using AddonLib.Configuration;

namespace AddonLib
{
    public interface IAddonSource
    {
        string Name { get; }
        string Url { get; }
        
        bool CanHandle(Addon addon);
        byte[] Download(Addon addon, IProgressHandler progressHandler);
        int GetVersion(Addon addon);
    }
}