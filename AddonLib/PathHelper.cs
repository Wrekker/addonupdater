﻿using System.IO;

namespace AddonLib
{
    public class PathHelper
    {
        public static void CopyRecursive(string source, string destination) {
            if (!Directory.Exists(source)) {
                return;
            }
            
            if (!Directory.Exists(destination)) {
                Directory.CreateDirectory(destination);
            }
            
            DirectoryInfo sourceDir = new DirectoryInfo(source);
            foreach (var fsi in sourceDir.GetFileSystemInfos()) {
                if ((fsi.Attributes & FileAttributes.Directory) == FileAttributes.Directory) {
                    CopyRecursive(
                        Path.Combine(source, fsi.Name),
                        Path.Combine(destination, fsi.Name));
                    continue;
                }
                
                File.Copy(fsi.FullName, Path.Combine(destination, fsi.Name), true);
            }
        }
    }
}