using System.IO;
using System.Linq;

namespace AddonLib
{
    using ICSharpCode.SharpZipLib.Zip;

    public class ZipHelper
    {
        private readonly string _addonPath;

        public ZipHelper(string addonPath) {
            _addonPath = addonPath;
        }

        public string GetAddonBaseDir(ZipFile zip) {
            if (zip == null) {
                return null;
            }

            return Path.GetDirectoryName(zip.Cast<ZipEntry>().First().Name).Split('/', '\\')[0];
        }
        
        public void Truncate(ZipFile zip) {
            SimpleTree<string> dirTree = new SimpleTree<string>();

            for (int i = 0; i < zip.Count; i++) {
                var entry = zip[i];

                if (entry.IsDirectory)
                    continue;

                string normalizedName = entry.Name.Replace('/', Path.DirectorySeparatorChar);
                if (Path.GetDirectoryName(normalizedName) != null) {
                    string[] dirs = Path.GetDirectoryName(normalizedName).Split(Path.DirectorySeparatorChar);
                    dirTree.Insert(dirs);
                }
            }

            foreach (var d in dirTree.Children) {
                string dir = Path.Combine(_addonPath, d.Value);
                if (Directory.Exists(dir)) {
                    Directory.Delete(dir, true);
                }
            }
        }

        public void Unpack(ZipFile zip, IProgressHandler progressHandler)
        {
            foreach (var entry in zip)
            {
                if (entry.IsDirectory)
                    continue;

                string path = Path.Combine(_addonPath, Path.GetDirectoryName(entry.Name));
                string file = Path.Combine(_addonPath, entry.Name);

                Directory.CreateDirectory(path);

                using (var fs = new FileStream(file, FileMode.Create))
                using (var r = zip.GetInputStream(entry)) {
                    StreamHelper.Copy(r, fs, entry.CompressedSize, progressHandler.UpdateCompletion);
                }
            }
        }
    }
}