using System;
using System.IO;

namespace AddonLib
{
    public class StreamHelper
    {
        public static void Copy(Stream source, Stream dest, long sourceLength, Action<double> onProgress) {
            byte[] buffer = new byte[1024];
            int read;
            int totalRead = 0;
            int i = 1;

            onProgress(0);
            while ((read = source.Read(buffer, 0, buffer.Length)) > 0) {
                dest.Write(buffer, 0, read);
                totalRead += read;

                if (++i%100==0) {
                    onProgress(totalRead/(double) sourceLength);
                }
            }

            if (i%100 != 0) {
                onProgress(1);
            }
        }
    }
}