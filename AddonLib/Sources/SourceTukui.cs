﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text.RegularExpressions;
using AddonLib.Configuration;

namespace AddonLib.Sources
{
    public class SourceTukui : IAddonSource
    {
        private static readonly string CorePortalUrl = "http://www.tukui.org/dl.php";
        
        private static readonly string[] CoreUrls = {
                                                        "http://www.tukui.org/downloads/tukui",
                                                        "http://www.tukui.org/downloads/elvui"
                                                    };

        private static readonly string AddonDownloadUrl = "http://www.tukui.org/addons/index.php?act=download&id={0}";

        private static readonly CultureInfo ParseCultureInfo = new CultureInfo("en-US");

        public string Name { get { return "TukUI"; } }
        public string Url { get { return "http://www.tukui.org"; } }

        public bool CanHandle(Addon addon) {
            return Regex.IsMatch(addon.Url, @"^http://www\.tukui\.org/", RegexOptions.IgnoreCase);
        }

        public int GetVersion(Addon addon) {
            Regex rgxVersion;
            TinyWebClient webClient = new TinyWebClient();

            int index = Array.IndexOf(CoreUrls, addon.Url.ToLower());
            if (index >= 0) {
                // core

                string html = webClient.GetString(CorePortalUrl);
                rgxVersion = new Regex(@"<span class=""VIP"">\s*(?<version>\d+\.\d+)\s*</span>", RegexOptions.IgnoreCase);
                MatchCollection m = rgxVersion.Matches(html);

                return (int)(float.Parse(m[index].Groups["version"].Value, ParseCultureInfo) * 1000);
            }
            
            if (Regex.IsMatch(addon.Url, "^http://www.tukui.org/addons")) {
                // plug-in
                string html = webClient.GetString(addon.Url);
                rgxVersion = new Regex(@"Latest Update:.*?(?<date>\w{3} \d{1,2}&#44; \d{4} \d{2}:\d{2} (?:AM|PM))", RegexOptions.IgnoreCase | RegexOptions.Singleline);

                var updateDate = rgxVersion.Match(html).Groups["date"].Value.Replace("&#44;", ",");
                var date = DateTime.ParseExact(updateDate, "MMM dd, yyyy hh:mm tt", ParseCultureInfo);

                return (int)(date - new DateTime(1970, 1, 1)).TotalMinutes;
            }

            throw new ArgumentException("Not a valid TukUI addon.");
        }
        
        public byte[] Download(Addon addon, IProgressHandler progressHandler) {
            TinyWebClient webClient = new TinyWebClient();
            string html;

            int index = Array.IndexOf(CoreUrls, addon.Url.ToLower());
            if (index >= 0) {
                html = webClient.GetString(CorePortalUrl);
                progressHandler.UpdateStage();

                var dlUrl = Regex.Match(html, addon.Url + @"-(?:\d+(?:\.\d+)*)\.zip").Value;
                
                var data = webClient.GetStream(dlUrl);
                progressHandler.UpdateStage();

                using (var ms = new MemoryStream()) {
                    ResponseStream.Copy(data, ms, progressHandler.UpdateCompletion);
                    return ms.ToArray();
                }
            }

            var m = Regex.Match(addon.Url, @"^http://www\.tukui\.org/addons/index.php\?act=view&id=(?<id>\d+)");
            if (m.Success) {
                var id = int.Parse(m.Groups["id"].Value);
                var redirUrl = string.Format(AddonDownloadUrl, id);

                html = webClient.GetString(redirUrl);
                m = Regex.Match(html, @"http://www\.tukui\.org/addons/downloads/\d+/\d+/.+\.zip");
                var dlUrl = m.Value;
                
                var data = webClient.GetStream(dlUrl);
                progressHandler.UpdateStage();

                using (var ms = new MemoryStream()) {
                    ResponseStream.Copy(data, ms, progressHandler.UpdateCompletion);
                    return ms.ToArray();
                }
            }

            throw new ArgumentException("Not a valid TukUI addon.");
        }
    }
}