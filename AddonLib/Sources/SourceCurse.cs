﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using AddonLib.Configuration;

namespace AddonLib.Sources
{
    public class SourceCurse : IAddonSource
    {
        public string Name { get { return "Curse"; } }
        public string Url { get { return "http://www.curse.com"; } }

        public SourceCurse() { }

        public bool CanHandle(Addon addon) {
            return Regex.IsMatch(addon.Url, "^https://mods.curse.com/addons/wow/.*$")
                   || Regex.IsMatch(addon.Url, "^https://www.curseforge.com/wow/addons/.*$");
        }

        public int GetVersion(Addon addon) {
            Regex rgxVersion = new Regex("Last Updated:.*?data-epoch=\"(?<unix>\\d+)\">", RegexOptions.IgnoreCase);

            TinyWebClient webClient = new TinyWebClient();
            string html = webClient.GetString(addon.Url);
            Match m = rgxVersion.Match(html);

            return int.Parse(m.Groups["unix"].Value);
        }
        
        public byte[] Download(Addon addon, IProgressHandler progressHandler) {
            var addonKey = Regex
                .Match(addon.Url, @"^(?:https://mods.curse.com/addons/wow/|https://www.curseforge.com/wow/addons/)(?<name>.*)$")
                .Groups["name"]
                .Value;

            var webClient = new TinyWebClient();
            var html = webClient.GetString($"https://www.curseforge.com/wow/addons/{addonKey}/download");
            progressHandler.UpdateStage();

            var downloadPath = Regex
                .Match(html, $@"/wow/addons/{addonKey}/download/\d+/file")
                .Value;
            
            var data = webClient.GetStream($"https://www.curseforge.com/{downloadPath}");
            progressHandler.UpdateStage();

            using (var ms = new MemoryStream()) {
                ResponseStream.Copy(data, ms, progressHandler.UpdateCompletion);
                return ms.ToArray();
            }
        }
    }
}