﻿using System.IO;
using System.Text.RegularExpressions;
using AddonLib.Configuration;

namespace AddonLib.Sources
{
    public class SourceWowInterface : IAddonSource
    {
        private const string BaseUrl = "http://www.wowinterface.com/downloads/";

        public string Name { get { return "WoWInterface"; } }
        public string Url { get { return "http://www.wowinterface.com"; } }

        public SourceWowInterface() { }

        public bool CanHandle(Addon addon) {
            return Regex.IsMatch(addon.Url, "^" + BaseUrl + "info.*$");
        }

        public int GetVersion(Addon addon) {
            // <abbr.+?>\s*(?<major>\d+)\.(?<minor>\d+)\s*</abbr>

            // Updated: 06-14-13
            Regex rgxVersion = new Regex("Updated: (?<month>\\d+)-(?<day>\\d+)-(?<year>\\d+)", RegexOptions.IgnoreCase);

            TinyWebClient webClient = new TinyWebClient();
            string html = webClient.GetString(addon.Url);
            Match m = rgxVersion.Match(html);


            return int.Parse(m.Groups["year"].Value)*10000
                + int.Parse(m.Groups["month"].Value)*100
                + int.Parse(m.Groups["day"].Value);
        }
        
        public byte[] Download(Addon addon, IProgressHandler progressHandler) {
            // http://www.wowinterface.com/downloads/download12995-NPCScan
            var webClient = new TinyWebClient();
            
            string html = webClient.GetString(addon.Url);
            progressHandler.UpdateStage();
            var rgxRouteLink = new Regex(@"/downloads/(?<dl>dl\.php\?s=[\w\d]+&amp;id=\d+)", RegexOptions.IgnoreCase);
            var m = rgxRouteLink.Match(html);
            var link = BaseUrl + m.Groups["dl"].Value.Replace("&amp;", "&");
            
            var data = webClient.GetStream(link);
            progressHandler.UpdateStage();

            using (var ms = new MemoryStream()) {
                ResponseStream.Copy(data, ms, progressHandler.UpdateCompletion);
                return ms.ToArray();
            }
        }
    }
}