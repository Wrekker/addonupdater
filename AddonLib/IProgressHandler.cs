﻿namespace AddonLib
{
    public interface IProgressHandler
    {
        void UpdateCompletion(double progress);
        void UpdateStage();
    }
}