﻿using System;

namespace AddonLib
{
    public class ConsoleProgressHandler : IProgressHandler
    {
        private int _l;
        private int _t;

        public ConsoleProgressHandler() {
            _l = Console.CursorLeft;
            _t = Console.CursorTop;
        }

        public void UpdateCompletion(double progress) {
            Console.SetCursorPosition(_l, _t);
            Console.Write("{0}%", (Math.Round(progress, 2) * 100));
        }

        public void UpdateStage() {
            Console.Write(".");
            _l = Console.CursorLeft;
            _t = Console.CursorTop;
        }
    }
}