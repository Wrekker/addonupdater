﻿using System.Collections.Generic;
using System.Linq;

namespace AddonLib
{
    public class SimpleTree<T>
    {
        public T Value { get; private set; }
        public List<SimpleTree<T>> Children { get; private set; }

        public SimpleTree() : this(default(T)) { }
        private SimpleTree(T value) {
            this.Value = value;
            this.Children = new List<SimpleTree<T>>();
        }

        public void Insert(IEnumerable<T> value) {
            if (!value.Any())
                return;

            foreach (var child in Children) {
                if (child.Value.Equals(value.First())) {
                    child.Insert(value.Skip(1));
                    return;
                }
            }

            var newTree = new SimpleTree<T>(value.First());
            newTree.Insert(value.Skip(1));
            this.Children.Add(newTree);
        }

    }
}