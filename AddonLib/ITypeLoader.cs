using System;
using System.Collections.Generic;

namespace AddonLib
{
    public interface ITypeLoader
    {
        IEnumerable<Type> LoadTypes(Type implementationsOf);
    }
}