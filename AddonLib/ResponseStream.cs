﻿using System;
using System.IO;

namespace AddonLib
{
    public class ResponseStream
    {
        public Stream Stream { get; private set; }
        public long ContentLength { get; private set; }

        public ResponseStream(Stream stream, long contentLength) {
            Stream = stream;
            ContentLength = contentLength;
        }

        public static ResponseStream Copy(ResponseStream source, Stream dest, Action<double> onProgress) {
            StreamHelper.Copy(source.Stream, dest, source.ContentLength, onProgress);
            return new ResponseStream(dest, source.ContentLength);
        }
    }
}